/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ejercicios;

import depuracion.*;
import java.util.Scanner;

/**
 *
 * @author luisnavarro Clase para ilustrar errores de ejecución, compilación,
 * linkado....
 */
public class EcuacionSegundoGrado {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Dime los coeficientes de una raiz de segundo grado, a,b, c tal que ax^2+bx+c=0 :");
        int a, b, c;
        int d = 0;
        try {
            a = teclado.nextInt();
            b = teclado.nextInt();
            c = teclado.nextInt();
            System.out.println("La ecuación a resolver es: " + a + "x^2+" + b + "x+" + c + "=0");
            //Veo el valor del determinante.... b*b-4*a*c
            String resultado = calculaRAicesEcuacionSEgundoGrado(a, b, c);
            System.out.println("=" + resultado);
        } catch (NumberFormatException nfe) {
//            jLabel2.setText("Se ha introducido un parámetro que no es un número"+nfe.getMessage());
            System.out.println("Se ha introducido un parámetro que no es un número"+nfe.getMessage());
            nfe.printStackTrace();
        } catch (ArithmeticException ae){
  //          jLabel2.setText("Ha habido un problema con los parámetros, que no es que no sean números"+ae.getMessage());
            System.out.println("Ha habido un problema con los parámetros, que no es que no sean números"+ae.getMessage());
            ae.printStackTrace();
            
        
        } catch (IllegalArgumentException iae){
    //        jLabel2.setText("Ha habido un problema con los parámetros, que no es que no sean números"+iae.getMessage());
            System.out.println("Ha habido un problema con los parámetros, que no es que no sean números"+iae.getMessage());
            iae.printStackTrace();
            
        
        } catch (Exception e){
      //      jLabel2.setText("Ha habido un problema con los parámetros, que no es que no sean números"+e.getMessage());
            System.out.println("Ha habido un problema con los parámetros, que no es que no sean números"+e.getMessage());
            e.printStackTrace();
            
        }
   }

    public static String calculaRAicesEcuacionSEgundoGrado(int a, int b, int c)  {
        int d = 0;
        //Veo el valor del determinante.... b*b-4*a*c
        d = b * b - 4 * a * c;
        if (d < 0) {
            return ("La ecuación no tiene solución");
        }
        if (d == 0) {
            return ("La ecuación tiene una única solución doble que es: " + (-b / (2 * a)));
        } else //d>0
        {
            return ("La ecuación tiene dos soluciones:" + (-b - Math.sqrt(d) / (2 * a)) + " " + (-b + Math.sqrt(d) / (2 * a)));
        }

    }

}
