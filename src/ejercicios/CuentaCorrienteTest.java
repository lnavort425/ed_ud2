/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import cuentacorriente.CuentaCorrienteV1;
import cuentacorriente.CuentaCorrienteV1;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luis Navarro
 */
public class CuentaCorrienteTest {
    private static CuentaCorrienteV1 cuentaDestino;
    private CuentaCorrienteV1 cuentaPrueba;
    final private int SALDO_INICIAL_CUENTA_PRUEBA=100;
    final static private int SALDO_INICIAL_CUENTA_DESTINO=1000;
    public CuentaCorrienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        cuentaDestino=new CuentaCorrienteV1("destino desconocido", SALDO_INICIAL_CUENTA_DESTINO);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        cuentaPrueba=new CuentaCorrienteV1("CuentaPRueba", SALDO_INICIAL_CUENTA_PRUEBA);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ingresa method, of class CuentaCorriente.
     */
    @Test
    public void testIngresa1() {
        System.out.println("ingresa");
        double ingreso = 200;
        //CuentaCorriente instance = new CuentaCorriente();
        cuentaPrueba.ingresa(ingreso);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(cuentaPrueba.getSaldo()==SALDO_INICIAL_CUENTA_PRUEBA+ingreso);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of extrae method, of class CuentaCorriente.
     */
    @Test
    public void testExtrae() {
        System.out.println("extrae");
        double e = 220.0;
        //CuentaCorriente instance = new CuentaCorriente();
        cuentaPrueba.extrae(e);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(cuentaPrueba.getSaldo()==SALDO_INICIAL_CUENTA_PRUEBA-e);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferir() {
        System.out.println("tranferir");
        //CuentaCorriente destino = null;
        double cantidadTransferir = 1000.0;
        double saldoInicialCuentaDEstino=cuentaDestino.getSaldo();
        //CuentaCorriente instance = new CuentaCorriente();
        cuentaPrueba.tranferir(cuentaDestino, cantidadTransferir);
        assertTrue((cuentaPrueba.getSaldo()==SALDO_INICIAL_CUENTA_PRUEBA-cantidadTransferir)&&(cuentaDestino.getSaldo()==saldoInicialCuentaDEstino+cantidadTransferir));
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ingresa method, of class CuentaCorriente.
     */
    @Test
    public void testIngresa() {
        System.out.println("ingresa");
        double i = 0.0;
        CuentaCorrienteV1 instance = new CuentaCorrienteV1();
        instance.ingresa(i);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class CuentaCorriente.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CuentaCorrienteV1 instance = new CuentaCorrienteV1();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSaldo method, of class CuentaCorriente.
     */
    @Test
    public void testGetSaldo() {
        System.out.println("getSaldo");
        CuentaCorrienteV1 instance = new CuentaCorrienteV1();
        double expResult = 0.0;
        double result = instance.getSaldo();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getId method, of class CuentaCorriente.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        CuentaCorrienteV1 instance = new CuentaCorrienteV1();
        int expResult = 0;
        int result = instance.getId();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTitular method, of class CuentaCorriente.
     */
    @Test
    public void testGetTitular() {
        System.out.println("getTitular");
        CuentaCorrienteV1 instance = new CuentaCorrienteV1();
        String expResult = "";
        String result = instance.getTitular();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTitular method, of class CuentaCorriente.
     */
    @Test
    public void testSetTitular() {
        System.out.println("setTitular");
        String aTitular = "";
        CuentaCorrienteV1 instance = new CuentaCorrienteV1();
        instance.setTitular(aTitular);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
