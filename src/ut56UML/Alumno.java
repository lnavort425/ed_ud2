/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut56UML;

import java.time.LocalDate;

/**
 *
 * @author luisnavarro
 */
/**
 * Clase que representa a un alumno
 */
public class Alumno extends Persona {

    private String grupo;
    private double notaMedia;

    /**
     * Representación en forma de String del contenido del objeto Alumno
     * Aprovecha el método toString de la clase Persona mediante una llamada a
     * super.toString(). Es decir, se está ampliando la funcionalidad de la
     * clase Persona.
     *
     * @return Representación textual del contenido de un objeto Alumno
     */
    public Alumno(String nombre, String apellidos, LocalDate fechaNacimiento, String grup, double notaMedi){
        super(nombre,apellidos,fechaNacimiento);
        this.grupo=grup;
        this.notaMedia=notaMedi;
    }
    @Override
    public String toString() {

        // Llamada al método “toString” (método "homónimo") de la superclase
        String infoPadre = super.toString();

        // A continuación añadimos la información “especializada” de esta subclase
        return String.format("%sGrupo: %s\nNota media: %.2f",
                infoPadre,
                this.grupo,
                this.notaMedia);
    }
}
