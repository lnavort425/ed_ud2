/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut56UML;

import java.text.SimpleDateFormat;
import java.time.LocalDate;

/**
 *
 * @author luisnavarro
 */
/**
 * Clase que representa a una persona
 */
public class Persona {

    private String nombre;
    private String apellidos;
    private LocalDate fechaNacimiento;

    /**
     * Representación en forma de String del contenido del objeto Persona
     *
     * @return String que representa el contenido del objeto Persona
     */

    public Persona(String nombre, String Apellidos, LocalDate fechaNac) {
        this.nombre = nombre;
        this.apellidos = Apellidos;
        this.fechaNacimiento = fechaNac;
    }

    public String toString() {
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String stringFecha = formatoFecha.format(this.fechaNacimiento);

        return String.format("Nombre: %s\nApellidos: %s\nFecha de nacimiento: %s",
                this.nombre, this.apellidos, stringFecha);

    }
}
