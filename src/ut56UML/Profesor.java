/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut56UML;

import java.time.LocalDate;

/**
 *
 * @author luisnavarro
 */
public class Profesor extends Persona {

    /**
     * Constructor de la clase Profesor
     *
     * @param nombre Nombre del profesor
     * @param apellidos Apellidos del profesor
     * @param fechaNacimiento Fecha de nacimiento del profesor
     * @param especialidad Especialidad del profesor
     * @param salario Salario del profesor
     */
    private String especialidad;
    private double salario;

    public Profesor(String nombre, String apellidos, LocalDate fechaNacimiento, String especialidad, double salario) {
        super(nombre, apellidos, fechaNacimiento);
        this.especialidad = especialidad;
        this.salario = salario;
    }
    public Profesor(String nombre) {
        this(nombre, nombre, LocalDate.now(),"unknown",2222);
    }
}
