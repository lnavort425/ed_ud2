/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ut56UML;

/**
 *
 * @author Luis Navarro
 */

public class Cuadrado extends Figura{

private double lado;

public Cuadrado (double x, double y, String color, double lado) {

super (x, y, color);
this.lado = lado;
}

public double getLado () {
return lado;
}

public void setLado (double lado){
this.lado = lado;
}

public double area () {
return lado * lado;
}

public double perimetro() {
return lado * 4;
}



}
