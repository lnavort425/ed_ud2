/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ut2;

/**
 *
 * @author luisnavarro2
 */
public class ConversorMonedas {
    private static double ratioEuroDolar=1.1;
    public static double deEurosADolares(double euros){
        return euros*getRatioEuroDolar();
    }
    public static double deDolaresAEuros(double dolares){
        return dolares/getRatioEuroDolar();
    }

    /**
     * @return the ratioEuroDolar
     */
    public static double getRatioEuroDolar() {
        return ratioEuroDolar;
    }

    /**
     * @param aRatioEuroDolar the ratioEuroDolar to set
     */
    public static void setRatioEuroDolar(double aRatioEuroDolar) {
        ratioEuroDolar = aRatioEuroDolar;
    }
}
