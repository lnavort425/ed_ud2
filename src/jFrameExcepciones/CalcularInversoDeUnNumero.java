/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package jFrameExcepciones;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class CalcularInversoDeUnNumero {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        while(true){
            System.out.println("Dime un número y te digo su inverso:");
            int n=teclado.nextInt();
            if (n==0) throw new ArithmeticException("Division por cero");
            System.out.println("El inverso es"+(double)1/n);
        }
    }
   
}
