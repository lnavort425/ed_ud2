/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1dammut7Ejemplos;

import java.time.LocalDate;

/**
 *
 * @author Luis Navarro
 */
/**
 * Clase que representa a un alumno.
 * Hereda de la clase Persona.
 */
public class Alumno extends Persona  {
    protected String grupo;
    protected double notaMedia; 


    // Constructor
    // -----------
    /**
     * Constructor de la clase Alumno   
     * @param nombre          Nombre del alumno
     * @param apellidos       Apellidos del alumno
     * @param fechaNacimiento Fecha de nacimiento del alumno
     * @param grupo           Grupo al que pertenece el alumno
     * @param notaMedia       Nota media del alumno
     */    
    public Alumno (String nombre, String apellidos,
            LocalDate fechaNacimiento, String grupo, 
            double notaMedia) {
        super (nombre, apellidos, fechaNacimiento);
        this.grupo= grupo;
        this.notaMedia= notaMedia;            
    }


    /** 
     * Getter del grupo
     * @return Grupo al que pertenece el alumno
     */
    public String getGrupo (){
        return grupo;
    }

    /**
     * Getter de la nota media
     * @return Nota Nota media del alumno
     */
    public double getNotaMedia (){
        return notaMedia;
    }

    /**
     * Setter del grupo
     * @param grupo Grupo al que pertenece el alumno
     */
    public void setGrupo (String grupo){
        this.grupo= grupo;
    }

    /** 
     * Setter de la nota media
     * @param notaMedia Nota media del alumno
     */
    public void setNotaMedia (double notaMedia){
        this.notaMedia= notaMedia;
    }
    
        public void saluda()
    {
        System.out.println("hola, soy "+nombre+" y nací "+fechaNacimiento+" y estoy en el grupo "+grupo+" y mi nota media es:"+notaMedia);
    }
}