/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr1dammut7Ejemplos;

import java.time.LocalDate;

/**
 *
 * @author Luis Navarro
 */
/**
 * Clase abastracta Persona
 * Clase que representa a una persona.
 * No es instanciable.
 */
public abstract class Persona {
    protected String nombre;
    protected String apellidos;
    protected LocalDate fechaNacimiento;

    // Constructores
    // -------------

    /**
     * Constructor de la clase Persona
     * @param nombre            Nombre de la persona
     * @param apellidos         Apellidos de la persona
     * @param fechaNacimiento   Fecha de nacimiento de la persona
     */
    public Persona (String nombre, String apellidos,
            LocalDate fechaNacimiento) {           
        this.nombre= nombre;
        this.apellidos= apellidos;
        this.fechaNacimiento= fechaNacimiento;
    }

    /**
     * Getter del atributo nombre
     * @return Nombre de la persona
     */
    protected String getNombre (){
        return nombre;
    }

    /**
     * Getter del atributo apellidos
     * @return Apellidos de la persona
     */
    protected String getApellidos (){
        return apellidos;
    }

    /**
     * Getter de la fecha de nacimiento
     * @return Fecha de nacimiento de la persona
     */
    protected LocalDate getFechaNacimiento (){
        return this.fechaNacimiento;
    }

    /**
     * Setter del nombre
     * @param nombre Nombre de la persona
     */
    protected void setNombre (String nombre){
        this.nombre= nombre;
    }

    /**
     * Setter de los apellidos
     * @param apellidos Apellidos de la persona
     */
    protected void setApellidos (String apellidos){
        this.apellidos= apellidos;
    }

    /**
     * ¨Setter de la fecha de nacimiento
     * @param fechaNacim Fecha de nacimiento de la persona
     */
    protected void setFechaNacimiento (LocalDate fechaNacimiento){
        this.fechaNacimiento= fechaNacimiento;
    }
    protected void saluda()
    {
        System.out.println("hola, soy "+nombre+" y nací "+fechaNacimiento);
    }
}