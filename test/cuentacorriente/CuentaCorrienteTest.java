/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuentacorriente;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luis Navarro
 */
public class CuentaCorrienteTest {

    static CuentaCorrienteV1 destino;
    CuentaCorrienteV1 origen;

    public CuentaCorrienteTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        destino = new CuentaCorrienteV1("CuentaDestino", 1000);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        origen = new CuentaCorrienteV1("CuentaPrueba", 500);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of ingresa method, of class CuentaCorriente.
     */
    @Test
    public void testIngresa() {
        System.out.println("ingresa1");
        double cantidad = 200;
        //CuentaCorriente instance = new CuentaCorriente(500);
        double saldoInicial = origen.getSaldo();
        origen.ingresa(cantidad);
        assertTrue(origen.getSaldo() == saldoInicial + cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of extrae method, of class CuentaCorriente.
     */
    @Test
    public void testExtrae() {
        System.out.println("extrae");
        double e = 200.0;
        //CuentaCorriente instance = new CuentaCorriente();
        double saldoInicial = origen.getSaldo();
        origen.extrae(e);
        // TODO review the generated test code and remove the default call to fail.
        assertTrue(origen.getSaldo() == saldoInicial - e);
    }

    /**
     * Test of extrae method, of class CuentaCorriente.
     */
    @Test
    public void testExtrae2() {
        System.out.println("extrae2");
        //CuentaCorriente instance = new CuentaCorriente();
        double saldoInicial = origen.getSaldo();
        double e = 200.0 + saldoInicial;
        try {
            origen.extrae(e);
            // TODO review the generated test code and remove the default call to fail.
            System.out.println("no ha saltado la excepción ");
            //fail("The test case is a prototype.");
            assertTrue(false);
        } catch (Exception ex) {
            assertTrue(origen.getSaldo() == saldoInicial);
        }

    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferir() {
        System.out.println("tranferir");
        //CuentaCorriente destino = null;
        double cantidad = 200;
        double saldoInicialCuentaOrigen = origen.getSaldo();
        double saldoInicialCuentaDestino = destino.getSaldo();
        //CuentaCorriente instance = new CuentaCorriente();
        origen.tranferir(destino, cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        assertTrue((origen.getSaldo() == saldoInicialCuentaOrigen - cantidad) && (destino.getSaldo() == saldoInicialCuentaDestino + cantidad));

    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     */
    @Test
    public void testTranferir2() {
        System.out.println("tranferir2: Voy a intentar transferir más dinero del que tiene la cuenta origen");
        //CuentaCorriente destino = null;
        double cantidad = 200;
        double saldoInicialCuentaOrigen = origen.getSaldo();
        double saldoInicialCuentaDestino = destino.getSaldo();
        //CuentaCorriente instance = new CuentaCorriente();
        try {
            origen.tranferir(destino, cantidad);
            //si no salta la prueba es incorrecta, la marco para fallar
            assertTrue(false);
        } catch (Exception e) {
            //si la excepción salta, vamos bien, pero he de comprobar que no ha modificado los saldosﬁﬁ
            assertTrue((origen.getSaldo() == saldoInicialCuentaOrigen) && (destino.getSaldo() == saldoInicialCuentaDestino));
        }
        // TODO review the generated test code and remove the default call to fail.

    }
}
