/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuentacorriente;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Luis Navarro
 */
public class AntiguaCuentaCorrienteTest {
    CuentaCorrienteV1 miCuenta;
    static CuentaCorrienteV1 cuentaDestino;
    public AntiguaCuentaCorrienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        //al empezar las pruebas
        cuentaDestino=new CuentaCorrienteV1("cuenta destrinio para pruebas.",1333);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        miCuenta=new CuentaCorrienteV1("mi cuenta ",1000);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of ingresa method, of class CuentaCorriente.
     */
    @Test
    public void testIngresa() {
        System.out.println("ingresa");
        double cantidad = 100.0;
        double saldoInicial=miCuenta.getSaldo();
        //CuentaCorriente instance = new CuentaCorriente();
        miCuenta.ingresa(cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        assertTrue(miCuenta.getSaldo()==saldoInicial+cantidad);
    }

    /**
     * Test of extrae method, of class CuentaCorriente.
     * Prueba extrayendo menos de lo que tengo
     */
    @Test
    public void testExtrae() {
        System.out.println("extrae");
        double saldoInicial=miCuenta.getSaldo();
        double cantidad = saldoInicial-100.0;
        //CuentaCorriente instance = new CuentaCorriente();
        miCuenta.extrae(cantidad);//ingresa(cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        assertTrue(miCuenta.getSaldo()==saldoInicial-cantidad);
    }
    /**
     * Test of extrae method, of class CuentaCorriente.
     *      * Prueba extrayendo menos de lo que tengo
     */
    @Test
    public void testExtrae2() {
        System.out.println("extrae2");
        double saldoInicial=miCuenta.getSaldo();
        double cantidad = saldoInicial+100.0;
        try{
            miCuenta.extrae(cantidad);
            System.out.println("NO ..... Ha lanzado una excepción al intentar sacar más de lo que tengo, como esperaba");
        }catch(IllegalArgumentException iae){
                System.out.println("Ha lanzado una excepción al intentar sacar más de lo que tengo, como esperaba");
        }
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        assertTrue(miCuenta.getSaldo()==saldoInicial);
    }

    /**
     * Test of tranferir method, of class CuentaCorriente.
     * PRobar que la transferencia funciona: transfiero menos de mi saldo
     */
    @Test
    public void testTranferir() {
        System.out.println("tranferir");
                System.out.println(miCuenta);
        System.out.println(cuentaDestino);
         double saldoInicialmiCuenta=miCuenta.getSaldo();
         double saldoInicialCuentaDestino=cuentaDestino.getSaldo();
        double cantidad = saldoInicialmiCuenta-100.0;
        miCuenta.tranferir(cuentaDestino, cantidad);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
        System.out.println(miCuenta);
        System.out.println(cuentaDestino);
        assertTrue((miCuenta.getSaldo()==saldoInicialCuentaDestino-cantidad)&&(cuentaDestino.getSaldo()==saldoInicialCuentaDestino+cantidad));
   }


}
